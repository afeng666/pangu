-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.18 - MySQL Community Server - GPL
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for world_info
CREATE DATABASE IF NOT EXISTS `world_info` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `world_info`;

-- Dumping structure for table world_info.cnarea_2018
CREATE TABLE IF NOT EXISTS `cnarea_2018` (
  `id` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,
  `level` tinyint(1) unsigned NOT NULL COMMENT '层级',
  `parent_code` bigint(14) unsigned NOT NULL DEFAULT '0' COMMENT '父级行政代码',
  `area_code` bigint(14) unsigned NOT NULL DEFAULT '0' COMMENT '行政代码',
  `zip_code` mediumint(6) unsigned zerofill NOT NULL DEFAULT '000000' COMMENT '邮政编码',
  `city_code` char(6) NOT NULL DEFAULT '' COMMENT '区号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `short_name` varchar(50) NOT NULL DEFAULT '' COMMENT '简称',
  `merger_name` varchar(50) NOT NULL DEFAULT '' COMMENT '组合名',
  `pinyin` varchar(30) NOT NULL DEFAULT '' COMMENT '拼音',
  `lng` decimal(10,6) NOT NULL DEFAULT '0.000000' COMMENT '经度',
  `lat` decimal(10,6) NOT NULL DEFAULT '0.000000' COMMENT '纬度',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx` (`area_code`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=778093 DEFAULT CHARSET=utf8 COMMENT='中国行政地区表';

-- Data exporting was unselected.

-- Dumping structure for table world_info.orderlist
CREATE TABLE IF NOT EXISTS `orderlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '0=未支付\r\n1 已支付\r\n2=已完成\r\n3=已退货\r\n10=固化',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `addressid` int(11) NOT NULL DEFAULT '0' COMMENT '收获地址id',
  `order_detail_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table world_info.order_detail
CREATE TABLE IF NOT EXISTS `order_detail` (
  `order_detail_id` int(11) DEFAULT NULL,
  `shopid` int(11) DEFAULT NULL,
  `shopname` varchar(50) DEFAULT NULL,
  KEY `order_detail_id` (`order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table world_info.order_tmp
CREATE TABLE IF NOT EXISTS `order_tmp` (
  `id` int(11) DEFAULT NULL,
  `Column 2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='构思, 将所有创建的订单放这儿, 若是支付则移除is_pay=1';

-- Data exporting was unselected.

-- Dumping structure for table world_info.prop
CREATE TABLE IF NOT EXISTS `prop` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table world_info.scene
CREATE TABLE IF NOT EXISTS `scene` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table world_info.shop
CREATE TABLE IF NOT EXISTS `shop` (
  `shopid` int(11) NOT NULL,
  `shopname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`shopid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品表';

-- Data exporting was unselected.

-- Dumping structure for table world_info.user
CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '名称',
  `nickname` varchar(50) DEFAULT NULL COMMENT '称号',
  `age` int(1) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `cityid` int(10) DEFAULT NULL COMMENT '城市id，准确优先级高于city',
  `description` varchar(50) DEFAULT NULL,
  `description2` varchar(50) DEFAULT NULL COMMENT '描述2',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table world_info.video_camera
CREATE TABLE IF NOT EXISTS `video_camera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `addr` varchar(50) NOT NULL,
  `domain` varchar(50) NOT NULL,
  `host` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
