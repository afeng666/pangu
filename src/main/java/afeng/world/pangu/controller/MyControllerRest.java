package afeng.world.pangu.controller;

import afeng.world.pangu.data.Actor;
import afeng.world.pangu.data.Data;
import afeng.world.pangu.data.OrderList;
import afeng.world.pangu.service.OCRservice;
import afeng.world.pangu.service.OrderService;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class MyControllerRest {

    @Autowired
    OCRservice ocRservice;

    @Autowired
    Data data;
    @Resource
    OrderService orderService;

    @RequestMapping("/getImageWord")
    public String getWorldData(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        org.springframework.core.io.Resource resource = multipartFile.getResource();
        byte[] bytes = multipartFile.getBytes();
        JSONArray test = ocRservice.test(bytes);
        return test.toString();
    }
    @RequestMapping("/getWorldData")
    public Map<Point, List<Actor>> getWorldData() throws IOException {
//        ocRservice.test();

        return data.getWorldData();
    }


    /**
     * spring mvc样例 散仙
     * http..../sanxian?name=xxx&id=xx
     */
    @RequestMapping("/sanxian")
    public String sanxian(String name, String id) {
        return name + "--" + id;
    }

    @RequestMapping("/pathValue/{key}")
    public String pathValue(@PathVariable(value = "key") String key) {
        return key;
    }

    /**
     * spring mvc样例 地仙
     * http..../dixian?ID=11&name=xx
     */
    @RequestMapping("dixian")
    public String dixian(@RequestBody Actor actor) {

        return actor.toString();
    }

    @GetMapping("/currentOnlineNum")
    public String getTotalUser(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie;
        try {
            // 把 sessionId 记录在浏览器中
            cookie = new Cookie("JSESSIONID", URLEncoder.encode(request.getSession().getId(), "utf-8"));
            cookie.setPath("/");
            //设置 cookie 有效期为 2 天，设置长一点
            cookie.setMaxAge( 48*60 * 60);
            response.addCookie(cookie);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Integer count = (Integer) request.getSession().getServletContext().getAttribute("count");
        return "当前在线人数：" + count;
    }



    @RequestMapping("addOrder")
    public String addOrder(){
        int i = orderService.addOrder();
        log.info("插入了{}条数据",i);
        return "插入了" + i + "条数据";
    }
    @RequestMapping("seletOrder")
    public OrderList seletOrder(){
        OrderList orderList = orderService.selectOrder();
        return orderList;
    }

    @NacosValue(value = "${nacos.test.propertie:123}", autoRefreshed = true)
    private String testProperties;
    @GetMapping("/test")
    public String test(){
        return testProperties;
    }
}
