package afeng.world.pangu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @datetime: 2020/9/6 21:02
 * @project_name: pangu
 * @author: afeng
 */
@RestController
public class RedisTestController {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @RequestMapping("redisTest")
    public String test(){

        RedisConnectionFactory connectionFactory = stringRedisTemplate.getConnectionFactory();

        stringRedisTemplate.opsForValue().set("test","testV");
        return stringRedisTemplate.opsForValue().get("test");
    }
}
