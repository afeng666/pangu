package afeng.world.pangu.controller;

import afeng.world.pangu.data.Result;
import afeng.world.pangu.data.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @datetime: 2020/9/5 14:53
 * @project_name: pangu
 * @author: afeng
 */
@RestController
public class UserController {

    // 这样的数据都可以惊醒生成!!!
    @RequestMapping(value = "/user",method = RequestMethod.POST,params = "")
    public Result<Boolean> create(@RequestBody User user) {
        // TODO: 2020/9/5 参数校验
        return new Result<Boolean>();
    }
    @RequestMapping(value = "/user",method = RequestMethod.GET)
    public Result<User> query(@RequestBody User user) {
        return null;
    }
    @RequestMapping(value = "/user",method = RequestMethod.PUT)
    public Result<Boolean> update(@RequestBody User user) {
        return null;
    }
    @RequestMapping(value = "/user",method = RequestMethod.DELETE)
    public Result<Boolean> delete(@RequestBody User user) {
        return null;
    }
}
