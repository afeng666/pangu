package afeng.world.pangu.controller;

import afeng.world.pangu.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @datetime: 2020/9/6 17:59
 * @project_name: pangu
 * @author: afeng
 */
//@RestController
public class MailSendController {

    @Autowired
    private MailService mailService;

    @RequestMapping("mail")
    public boolean sendMailTest() {
        mailService.sendTest();
        return true;
    }
}
