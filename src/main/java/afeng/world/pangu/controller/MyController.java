package afeng.world.pangu.controller;

import afeng.world.pangu.data.OrderList;
import afeng.world.pangu.data.User;
import afeng.world.pangu.mapper.OrderListMapper;
import afeng.world.pangu.mapper.UserMapper;
import afeng.world.pangu.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class MyController {

    @RequestMapping(value = "/ocr", method = RequestMethod.GET)
    public String ocr() {
        return "ocr";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String defaultLogin() {
        return "system/login";
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestParam("username") String username, @RequestParam("password") String password) {
        // 从SecurityUtils里边创建一个 subject
        Subject subject = SecurityUtils.getSubject();
        // 在认证提交前准备 token（令牌）
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        // 执行认证登陆
        try {
            subject.login(token); // -->到realm中确认
        } catch (UnknownAccountException uae) {
            return "未知账户";
        } catch (IncorrectCredentialsException ice) {
            return "密码不正确";
        } catch (LockedAccountException lae) {
            return "账户已锁定";
        } catch (ExcessiveAttemptsException eae) {
            return "用户名或密码错误次数过多";
        } catch (AuthenticationException ae) {
            return "用户名或密码不正确！";
        }
        if (subject.isAuthenticated()) {
            return "登录成功";
        } else {
            token.clear();
            return "登录失败";
        }
    }


    /**
     * 单文件上传
     *
     * @param multipartFile
     */
    @RequestMapping("upload")
    public void upload(@RequestParam("file") MultipartFile multipartFile) {
        if (multipartFile.isEmpty()) {

        }
        String fileName = multipartFile.getOriginalFilename();
//            multipartFile.transferTo(dest);

    }

    @RequestMapping("uploadFiles")
    public void uploadFiles(HttpServletRequest httpServletRequest) {
//        String s = ;
//        ((MultipartHttpServletRequest)httpServletRequest).getFiles(s)
        List<MultipartFile> files = ((MultipartHttpServletRequest) httpServletRequest).getFiles("file");

    }

    @RequestMapping("down")
    public void down(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        try {
            int copy = IOUtils.copy(new FileInputStream(new File("")), httpServletResponse.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * mybatis测试
     */
    @Resource
    UserMapper userMapper;

    @RequestMapping("testMybatis")
    public void testUserMapper() {
        User user = userMapper.selectByPrimaryKey(1);
        log.info(user.toString());
    }

    @RequestMapping("helloThymeleaf")
    public String hello(String name, Model model) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        model.addAttribute("name", name);
        model.addAttribute("strList", list);
        model.addAttribute("show", false);
        return "helloTemplate";
    }


}
