package afeng.world.pangu.mapper;

import afeng.world.pangu.data.OrderDetail;

public interface OrderDetailMapper {
    int insert(OrderDetail record);

    int insertSelective(OrderDetail record);
}