package afeng.world.pangu.data;

import lombok.Data;

import java.awt.*;

@Data
public class Pawn extends Actor {

    int init_v = 10; //初始跳跃速度
    int g = 10; //重力加速度

    public Pawn(int ID, String name, Image image, Point point) {
        super(ID, name, image, point);
    }

    class Task extends Behavior {
        @Override
        public Object call() throws Exception {
            return null;
        }
    }

}
