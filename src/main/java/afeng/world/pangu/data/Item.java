package afeng.world.pangu.data;

public enum Item {

    HuangMengJi(1,"1","吃的黄猛击","asset/title.png"),
    Enemy(2,"1","吃的黄猛击","asset/img/actor/fbboss00095.png"),
    Luo(3,"1","吃的黄猛击","asset/img/actor/word_1.png"),
    zhangsan(4,"1","吃的黄猛击","asset/img/actor/fbboss00209.png"),
    HuangMengJi2(5,"1","吃的黄猛击","asset/img/actor/fbboss00245.png"),
    HuangMengJi23(6,"1","吃的黄猛击","asset/img/actor/fbboss00246.png"),
    HuangMengJi3(6,"1","吃的黄猛击","asset/img/actor/fbboss002001.png"),
    PAWN(8,"8","吃的黄猛击","asset/img/sf05.png"),
    HuangMengJi44(7,"1","吃的黄猛击","asset/img/actor/fbboss00331.png");

    private int id;
    private String code;
    private String description;
    private String imagePath;

    Item(int id, String code, String description,String imagePath) {

        this.id = id;
        this.code = code;
        this.description = description;
        this.imagePath = imagePath;
    }

    public static Item getValue(int value) {
        Item[] items = values();
        for (Item item : items) {
            if (item.getID()==value) {
                return item;
            }
        }
        return null;
    }

    public Integer getID(){
        return this.id;
    }

    public String getImagePath(){
        return this.imagePath;
    }

    public String desc(){
        return this.description;
    }
}
