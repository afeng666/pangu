package afeng.world.pangu.data;

import afeng.world.pangu.common.PanguConstants;
import afeng.world.pangu.tools.DisplayInfo;
import afeng.world.pangu.tools.PictureTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@lombok.Data
@Component
public class Data {
    /**
     * 凡是坐标都是网格,实际的坐标在进行计算
     */
    int wUnit = DisplayInfo.getScreenWith()/ PanguConstants.EXP_Factor;
    int hUnit = DisplayInfo.getScreenHeight()/ PanguConstants.EXP_Factor;
    /**
     * 网格坐标 -> actors(有重叠), list中后来居上,渲染最后的actor
     */
    Map<Point, List<Actor>> worldData = new ConcurrentHashMap();

    /**
     * 猪脚初始坐标
     */
    Point pawnPoint = new Point(wUnit/2,hUnit-20);


    @PostConstruct
    public void init(){
        /**
         * 初始化地图actor数据
         */
        Random random = new Random();
        int itemLength = Item.values().length;
        for (int w = 0; w < wUnit; w++) {
            for (int h  = 0; h < hUnit; h++) {
                if (Math.random() < PanguConstants.PRODUCT_DENSITY) {
                    int id = random.nextInt(itemLength);
                    log.debug("创建actorid="+id);
                    Item itemValue = Item.getValue(id);
                    if (itemValue == null) {
                        continue;  // 注意,之前写成return直接返回的bug
                    }
                    Point point = new Point(w, h);
                    Actor actor = new Actor(itemValue.getID(), itemValue.name(), PictureTools.getImage(itemValue.getImagePath()), point);
                    List<Actor> list = new ArrayList<>();
                    list.add(actor);
                    worldData.put(point, list);
                }
            }
        }
        /**
         * 初始化pawn信息
         */
        Pawn pawn = new Pawn(Item.PAWN.getID(), Item.PAWN.name(), PictureTools.getImage(Item.PAWN.getImagePath()), pawnPoint);
        List<Actor> list = worldData.get(pawnPoint);
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(pawn);
        worldData.put(pawnPoint, list);
        log.info("初始化地图actor数据完毕");
    }
}
