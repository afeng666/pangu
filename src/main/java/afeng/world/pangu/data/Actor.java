package afeng.world.pangu.data;

import afeng.world.pangu.tools.DisplayInfo;
import lombok.Data;

import java.awt.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Data
public class Actor{

    private int ID;
    private String name;
    private Image image;
    private Point point;

    public Actor(int ID, String name, Image image,Point point){

        this.ID = ID;
        this.name = name;
        this.image = image;
        this.point = point;
    }

    public class Behavior implements Callable {
        @Override
        public Object call() throws Exception {
//            try {
//                TimeUnit.MILLISECONDS.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            if (point.x > DisplayInfo.getScreenWith()) {
//                point.x = 0;
//            }
//            if (point.x < 0) {
//                point.x = DisplayInfo.getScreenWith();
//            }
//            if (point.y > DisplayInfo.getScreenHeight()) {
//                point.y = 0;
//            }
//            if (point.y < 0) {
//                point.y = DisplayInfo.getScreenHeight();
//            }
//            if (Math.random() < 0.5) {
//                point.x--;
//            } else {
//                point.x++;
//            }
//            if (Math.random() < 0.5) {
//                point.y--;
//            } else {
//                point.y++;
//            }
            return null;
        }
    }
}
