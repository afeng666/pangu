package afeng.world.pangu.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

@Slf4j
public class PictureTools {

    public static Image getImage(Resource resource) {
        URL url = null;
        Image Image = null;
        try {
            url = resource.getURL();
            Image = ImageIO.read(url);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return Image;
    }

    public static Image getImage(String imgPath) {
        Image image = null;
        try {
            ClassPathResource classPathResource = new ClassPathResource(imgPath);
            image = ImageIO.read(classPathResource.getURL());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return image;
    }

    public static BufferedImage getBufferedImage(String imgPath) {
        BufferedImage image = null;
        try {
            ClassPathResource classPathResource = new ClassPathResource(imgPath);
            image = ImageIO.read(classPathResource.getURL());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return image;
    }


}
