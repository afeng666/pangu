package afeng.world.pangu.tools;

import java.awt.*;

public class DisplayInfo {
    public static Toolkit defaultToolkit = Toolkit.getDefaultToolkit();

    public static Dimension getScreenSize() {
        Dimension screenSize = defaultToolkit.getScreenSize();
        return screenSize;
    }
    public static int getScreenWith() {
        return getScreenSize().width/4*3;
    }
    public static int getScreenHeight() {
        return getScreenSize().height/3*2;
    }

}
