package afeng.world.pangu.tools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class RobotTools {

    public static BufferedImage captureScreen() throws AWTException {
        Robot robot = new Robot();
        Dimension screenSize = DisplayInfo.getScreenSize();
        Rectangle screenRect = new Rectangle(screenSize);
        BufferedImage screenCapture = robot.createScreenCapture(screenRect);
        return screenCapture;
    }
    public static File writeTest(BufferedImage bufferedImage,String imageFileName) throws IOException {
        String path = "tmp/"+imageFileName;
        File file = new File(path);
        File fileParentFile = file.getParentFile();
        if (!fileParentFile.exists()) {
             file.mkdirs();
        }
        file.createNewFile();
        try {
            ImageIO.write(bufferedImage,"png",file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void openImage(File file){
    //打开图
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws AWTException, IOException {
        BufferedImage bufferedImage = captureScreen();
        File file = writeTest(bufferedImage, "capture.png");
        openImage(file);
    }
}
