package afeng.world.pangu.common.event;

/**
 * event --> listener --> handle
 *
 * 键盘,鼠标发生事件, windows进行捕获(监听), 然后发送到windows消息系统等待处理.
 */
public interface PanguEvent {

    public void sendEmail();
}
