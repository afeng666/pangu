package afeng.world.pangu.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

@Configuration
public class ThreadPoolManager {

    int i = Runtime.getRuntime().availableProcessors();
    ExecutorService executorService = new ThreadPoolExecutor(i, i,
            60L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>());
    @Bean
    public ExecutorService getExecutorService(){
        return executorService;
    }
}
