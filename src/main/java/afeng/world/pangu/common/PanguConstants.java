package afeng.world.pangu.common;

public class PanguConstants {
    public final static String NAME = "pangu";
    /**
     * 扩展系数
     */
    public final static int EXP_Factor = 10;
    /**
     * 生产密度
     */
    public final static float PRODUCT_DENSITY = 0.001f;
    /**
     * 行动方向
     */
    public final static int LEFT = 0;
    public final static int UP = 1;
    public final static int DOWN = 2;
    public final static int RIGHT = 3;
    public final static int JUMP = 4;

    /**
     * 纵向学习步
     */
    public final static int Y_pixel = 1;
    /**
     * 横向学习步
     */
    public final static int X_pixel = 1;
}
