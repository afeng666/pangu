package afeng.world.pangu.common;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
/**
 *  额外功能
 *  钉点
 *  切面
 */
public class LogHandler {

    /**
     * 钉点
     * 通过@Pointcut声明频繁使用的钉点
     * 表达式: 钉点位置计算
     */
    @Pointcut("execution(public * afeng.world.pangu.service.InputManager.*(..)))")
    public void BrokerAspect(){
    }

    @Pointcut("execution(* afeng.world.pangu.PanguApplication.main(..))")
    public void mainTest(){}

//    @Before("args(String,String)")
//    public void HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH(){
//        log.info("AOP钉子点进来了奇诡的东西!!!!");
//    }
//    @Before("@annotation()")
//    public void LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL(){
//        log.info("AOP钉子点进来了奇诡的东西!!!!");
//    }

    @Before("mainTest()")
    public void doBeforeLog(){
        log.info("AOP钉子点进来了奇诡的东西!!!!");
    }
    /**
     * @description  在连接点执行之前执行的通知
     */
    @Before("BrokerAspect()")
    public void doBeforeGame(){
        log.info("LogHandler前置切面");
    }

    /**
     * @description  在连接点执行之后执行的通知（返回通知和异常通知的异常）
     */
    @After("BrokerAspect()")
    public void doAfterGame(){
        System.out.println("LogHandler经纪人为球星表现疯狂鼓掌！");
    }

    /**
     * @description  在连接点执行之后执行的通知（返回通知）
     */
    @AfterReturning("BrokerAspect()")
    public void doAfterReturningGame(){
        System.out.println("LogHandler返回通知：经纪人为球星表现疯狂鼓掌！");
    }

    /**
     * @description  在连接点执行之后执行的通知（异常通知）
     */
    @AfterThrowing("BrokerAspect()")
    public void doAfterThrowingGame(){
        System.out.println("LogHandler异常通知：球迷要求退票！");
    }

    /**
     * @description  使用环绕通知
     */
    @Around("BrokerAspect()")
    public void doAroundGame(ProceedingJoinPoint pjp) throws Throwable {
        try{
            System.out.println("LogHandler经纪人正在处理球星赛前事务！");
            pjp.proceed();
            System.out.println("LogHandler返回通知：经纪人为球星表现疯狂鼓掌！");
        }
        catch(Throwable e){
            System.out.println("LogHandler异常通知：球迷要求退票！");
        }
    }
}
