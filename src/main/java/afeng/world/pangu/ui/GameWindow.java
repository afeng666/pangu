package afeng.world.pangu.ui;

import afeng.world.pangu.common.PanguConstants;
import afeng.world.pangu.data.Actor;
import afeng.world.pangu.tools.DisplayInfo;
import afeng.world.pangu.tools.PictureTools;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@Component
@Slf4j
@Data
@ConditionalOnBean(afeng.world.pangu.data.Data.class)
public class GameWindow extends JPanel {

    @Autowired // 这种注入的思想其实有点回归c语言, 和static方法, 将oop-->pop
    afeng.world.pangu.data.Data data;

    @Value("classpath:asset/img/title.png") //加入web后 要加classpath , 可能走得途径不一样, 待验 todo
    Resource titleRes;

    @Autowired
    ExecutorService executorService;

    JFrame jFrame = new JFrame();
    @PostConstruct
    public void init() {
        jFrame.setIconImage(PictureTools.getImage(titleRes));
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(DisplayInfo.getScreenWith(), DisplayInfo.getScreenHeight());
        jFrame.setLocationRelativeTo(null);
        jFrame.getContentPane().add(this);
        jFrame.setUndecorated(true);
        jFrame.setVisible(false);
    }

//    @Override
//    public void paint(Graphics g) {
//        super.paint(g);
//    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        UIinit(g);
        drawWorld(g);
//        drawPawn(g);
    }

    /**
     * 创建世界
     * @param g
     */
    private void drawWorld(Graphics g) {
        Map<Point, List<Actor>> worldData = data.getWorldData();
        for (Map.Entry<Point, List<Actor>> pointListEntry : worldData.entrySet()) {
            Point point = pointListEntry.getKey();
            List<Actor> actors = pointListEntry.getValue();
            Actor actor = actors.get(actors.size()-1);
            //test
//            if (actor.getID() == 1) {
//                Actor.Behavior behavior = actor.new Behavior();
//                Future submit = executorService.submit(behavior);
//                boolean cancel = submit.cancel(true);
//            }
            g.drawImage(actor.getImage(), point.x* PanguConstants.EXP_Factor, point.y* PanguConstants.EXP_Factor, this);
        }
    }

    /**
     * 创建猪脚
     * @param g
     */
    private void drawPawn(Graphics g) {
    }

    /**
     * 初始化界面
     * @param g
     */
    private void UIinit(Graphics g) {
        //背景色
        setBackground(Color.BLUE);
        g.setColor(Color.BLACK);
        g.fillRect(5,5,getWidth()-10,getHeight()-10);
        //边框
        Border titleBorder = new TitledBorder(null, PanguConstants.NAME,TitledBorder.LEADING,TitledBorder.BELOW_TOP,null,Color.RED);
        setBorder(titleBorder);
    }
}
