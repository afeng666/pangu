package afeng.world.pangu.ui;

import afeng.world.pangu.data.Item;
import afeng.world.pangu.tools.DisplayInfo;
import afeng.world.pangu.tools.PictureTools;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

//@Configuration
@Data
@Component
@Slf4j
public class Widages implements Runnable{

    @Value("asset/img/pawn.png")
    Resource pawn;

//    @Autowired
//    @Qualifier("back")
//    JPanel back; 循环引用了

    private Graphics graphics;
    private int x;
    private int y;
    private java.awt.Component component;
    @Bean(name = "pawn")
    public JPanel pawn() {

        int with = DisplayInfo.getScreenWith() / 6;
        int height = DisplayInfo.getScreenHeight() / 6;
        JPanel jPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                graphics = g;
                component = this;
//                try {
//                    ImageIcon imageIcon = new ImageIcon(pawn.getURL());
//                    imageIcon.paintIcon(this,g,this.getWidth()/2,this.getHeight()/2);
//                } catch (IOException e) {
//                    log.error(e.getMessage());
//                }
            }
        };
        jPanel.setSize(with, height);
        return jPanel;
    }

    @Bean(name = "back")
    public JPanel back(){
        int with = DisplayInfo.getScreenWith()/3;
        int height = DisplayInfo.getScreenHeight()/4;
        JPanel jPanel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
//                g.setColor(new Color(1,1,1));
//                g.fillRect(0,0,with,height);
                g.drawImage( PictureTools.getImage(Item.Luo.getImagePath()),0,0,this.getWidth(),this.getHeight(),null);
            }
        };
//        jPanel.setSize(with,height);
        jPanel.setBounds(5,5,with,height);
        return jPanel;
    }
    @Bean(name = {"shop"})
    public JPanel shop(){
        JPanel jPanel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

            }
        };
        return jPanel;
    }
    @Bean(name = {"skill"})
    public JPanel skill(){
        JPanel jPanel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

            }
        };
        return jPanel;
    }
    @Bean(name = {"personInfo"})
    public JPanel personInfo(){
        JPanel jPanel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

            }
        };
        return jPanel;
    }
    @Bean(name = {"itemInfo"})
    public JPanel itemInfo(){
        JPanel jPanel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

            }
        };
        return jPanel;
    }

    @Override
    public void run() {
        ImageIcon imageIcon = null;
        try {
            imageIcon = new ImageIcon(pawn.getURL());
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            x++;
            y++;
                imageIcon.paintIcon(component,graphics,x,y);
                log.info(component.toString());
                Container parent = component.getParent();
                parent.repaint();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
