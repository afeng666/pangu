package afeng.world.pangu.ui;

import afeng.world.pangu.tools.DisplayInfo;
import jdk.nashorn.internal.objects.annotations.Property;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

@Configuration
@ConfigurationProperties(prefix = "jframe")
@PropertySource(value = "classpath:application.yml")
@Slf4j
public class MainUI {

    @Property(name = "height")
    private int height;
    @Value("${with}")
    private int with;
    @Value("asset/title.png")
    Resource resource;

//    @Bean
    public JFrame jFrame() throws IOException {
        log.info("创建JFrame");
        JFrame jFrame = new JFrame();

        if (with == 0) {
            with = DisplayInfo.getScreenWith()/2;
        }
        if (height == 0) {
            height = DisplayInfo.getScreenHeight()/2;
        }
        Image image = ImageIO.read(resource.getURL());
        jFrame.setIconImage(image);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setBounds(with/2,height/2,with,height);
        jFrame.setUndecorated(true);
        return jFrame;
    }
}
