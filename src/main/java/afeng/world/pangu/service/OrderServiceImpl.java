package afeng.world.pangu.service;

import afeng.world.pangu.common.event.PanguEvent;
import afeng.world.pangu.data.OrderList;
import afeng.world.pangu.mapper.OrderListMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    OrderListMapper orderMapper;

    PanguEvent panguEvent = new PanguEvent() {
        @Override
        public void sendEmail() {
            log.info("发送邮件事件到kafka");
        }
    };

    @Override
    @Transactional
    public int addOrder() {

        /**
         * 添加order, 发送短信, 发送物流通知
         */
        OrderList order = new OrderList();
        order.setId(1);
        order.setAddressid(1);
        order.setName("test 商品");
        order.setState(1);
        order.setUid(1);
        order.setOrderDetailId(1);
        int insert = orderMapper.insert(order);
        panguEvent.sendEmail();
        return insert;
    }

    @Override
    public OrderList selectOrder() {
        OrderList orderList = orderMapper.selectByPrimaryKey(1);
        return orderList;
    }
}
