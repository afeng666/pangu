package afeng.world.pangu.service;

import afeng.world.pangu.common.PanguConstants;
import afeng.world.pangu.data.Actor;
import afeng.world.pangu.ui.GameWindow;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Component
@Data
@Slf4j
public class InputManager implements KeyListener, MouseListener,
        MouseMotionListener, MouseWheelListener {

    @Autowired
    afeng.world.pangu.data.Data data;

    private GameWindow gameWindow;

    @Autowired
    @Qualifier("back")
    JPanel back;
    @Autowired
    @Qualifier("shop")
    JPanel shop;

    public InputManager(@Qualifier("gameWindow") GameWindow gameWindow) {

        this.gameWindow = gameWindow;
        gameWindow.setLayout(null);
//        component.addKeyListener(this); jpanel不起作用
        gameWindow.getJFrame().addKeyListener(this);
        gameWindow.addMouseListener(this);
        gameWindow.addMouseMotionListener(this);
        gameWindow.addMouseWheelListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {

            case KeyEvent.VK_A:
                action(PanguConstants.LEFT);
                break;
            case KeyEvent.VK_D:
                action(PanguConstants.RIGHT);
                break;
            case KeyEvent.VK_ESCAPE:
                closeAllWidage();
                break;
            case KeyEvent.VK_B:
                openBack();
                break;
            case KeyEvent.VK_V:
                break;
            case KeyEvent.VK_K:
                break;
            case KeyEvent.VK_SPACE:
                action(PanguConstants.JUMP);
                break;
            default:
        }
        gameWindow.repaint();
    }

    private void action(int action) {
        Map<Point, List<Actor>> worldData = data.getWorldData();
        Point pawnPoint = data.getPawnPoint();//猪脚坐标
        List<Actor> list = worldData.get(pawnPoint); // 猪脚所在空间
        Actor actor = list.get(list.size() - 1); //猪脚

        Point targetPoint = new Point(pawnPoint);//目标地址
        if (action == PanguConstants.LEFT) {
            targetPoint.x = targetPoint.x - 1;
        }
        if (action == PanguConstants.UP) {
            targetPoint.y = targetPoint.y - 1;
        }
        if (action == PanguConstants.RIGHT) {
            targetPoint.x = targetPoint.x + 1;
        }
        int v = 10;
        int t = 0;
        int g = 10;
        if (action == PanguConstants.JUMP) {
            t++;
            int s = v * t - 1 / 2 * g * t * t;
            targetPoint.y = targetPoint.y + s;
        }

        List<Actor> targetList = worldData.get(targetPoint);//目标空间
        if (targetList == null) {
            targetList = new ArrayList<>();
        }
        //行动到target

        actor.setPoint(targetPoint); //猪脚坐标设为目标地址
        targetList.add(actor); //猪脚加到目标空间
        worldData.put(targetPoint, targetList);
        //清理原空间
        if (list.size() > 1) { //猪脚所在空间
            list.remove(list.size() - 1); //原空间删掉猪脚
        } else {
            worldData.remove(pawnPoint);
        }
        data.setPawnPoint(targetPoint); // 数据中心猪脚坐标
    }

    private void openBack() {
        gameWindow.add(back);
        gameWindow.revalidate();
    }

    private void closeAllWidage() {
        gameWindow.removeAll();
        gameWindow.revalidate();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Map<Point, List<Actor>> worldData = data.getWorldData();

        log.info("数据总计:" + data.getWorldData().size());
        gameWindow.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }
}
