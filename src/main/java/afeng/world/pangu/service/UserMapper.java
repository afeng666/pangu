package afeng.world.pangu.service;

import afeng.world.pangu.data.Actor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserMapper extends BaseMapper<Actor> {
}