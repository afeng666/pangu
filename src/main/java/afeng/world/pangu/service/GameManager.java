package afeng.world.pangu.service;

import afeng.world.pangu.service.SoundHandle;
import afeng.world.pangu.ui.GameWindow;
import afeng.world.pangu.ui.Widages;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.concurrent.*;

@Component
@Data
@Slf4j
public class GameManager {

    @Autowired
    @Qualifier(value = "back")
    JPanel back;
    @Autowired
    @Qualifier(value = "pawn")
    JPanel pawn;
    @Value("classpath:asset/img/pawn.png")
    Resource resource;
    @Autowired
    SoundHandle soundHandle;
    @Value("asset/sound/start.mp3")
    Resource startSoundResoure;

    @Autowired
    Widages widages;

//    @Autowired
//    @Qualifier("gameWindow")
//    GameWindow gameWindow;
//
//    int Num = Runtime.getRuntime().availableProcessors();
//    Executor executor = new ThreadPoolExecutor(Num, Num,
//            0L, TimeUnit.MILLISECONDS,
//            new LinkedBlockingQueue<Runnable>());
//
//    @PostConstruct
//    public void init(){
//        log.info("初始化GameManager");
//        gameWindow.repaint();
////        jFrame.getContentPane().setBackground(Color.BLACK);
////        jFrame.getContentPane().add(pawn);
////        jFrame.setVisible(true);
////        jFrame.repaint();
////        executor.execute(scene);
////        soundHandle.setResource(startSoundResoure);
//    }

}
