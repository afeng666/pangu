package afeng.world.pangu.service;

import afeng.world.pangu.ui.GameWindow;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.swing.*;

@Slf4j
@Component
public class UpdateUI {

    @Autowired
    @Qualifier("gameWindow")
    GameWindow gameWindow;

//    @Scheduled(fixedRate = 200)
    public void update(){
        log.debug("定时线程执行");
//        java.awt.Component[] components = jFrame.getContentPane().getComponents();
//        for (int i = 0; i < components.length; i++) {
//            components[i].setSize(components[i].getWidth()+1,components[i].getHeight());
////            components[i].repaint();
//        }
//        jFrame.repaint();
//        gameWindow.repaint();
    }
}
