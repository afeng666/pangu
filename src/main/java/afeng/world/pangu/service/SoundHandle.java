package afeng.world.pangu.service;

import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;
import javax.sound.midi.*;

/**
 * 音效处理
 */
@Component
public class SoundHandle implements Callable{

    ExecutorService executorService = Executors.newFixedThreadPool(1);
    Future submit;
    private Resource resource;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        submit.cancel(true);
        submit = executorService.submit(this);
    }

    private void play(Resource resource) {

    }

    @Override
    public Object call() throws Exception {
        play(resource);
        return null;
    }
}
