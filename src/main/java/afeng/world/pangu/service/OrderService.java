package afeng.world.pangu.service;

import afeng.world.pangu.data.OrderList;

public interface OrderService {

    /**
     * 添加订单
     */
    public int addOrder();
    /**
     * 查询订单
     * @return
     */
    public OrderList selectOrder();
}
