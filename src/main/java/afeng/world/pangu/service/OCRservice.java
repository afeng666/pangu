package afeng.world.pangu.service;

import com.baidu.aip.ocr.AipOcr;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

/**
 * @datetime: 2020/8/19 12:46
 * @project_name: pangu
 * @author: afeng
 */
@Component
@ConfigurationProperties(prefix = "baidu")
public class OCRservice {

    //设置APPID/AK/SK
    @Value("${baidu.APP_ID}")
    public   String APP_ID;
    @Value("${baidu.API_KEY}")
    public   String API_KEY ;
    @Value("${baidu.SECRET_KEY}")
    public   String SECRET_KEY ;

//    @Value("classpath:static/img/logo.png")
//    Resource resource;

    public  JSONArray test(byte[] bytes) throws IOException {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
//        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");

        // 调用接口
//        String path = resource.getURL().getPath();
        JSONObject res = client.basicGeneral(bytes, new HashMap<String, String>());
        JSONArray jsonArray = res.getJSONArray("words_result");
        System.out.println(res.toString(2));
        return jsonArray;

    }
}
