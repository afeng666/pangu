package afeng.world.pangu;

import afeng.world.pangu.data.Data;
//import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.io.IOException;

/**
 * @SpringBootApplication 配置, 自动配置, 扫描
 */
@SpringBootApplication
@Slf4j
@EnableScheduling
@EnableAsync // 开启异步调用
@ServletComponentScan
// afeng.world.pangu.mapper 不加的话,扫描范围大了, Controller层里的服务也被注入mybatis, 结果就出错
@MapperScan("afeng.world.pangu.mapper")
//@NacosPropertySource(dataId = "springboot2-nacos-config", autoRefreshed = true)
//@EnableDiscoveryClient
public class PanguApplication {

//    final static String CMD = "python src/python/pythonSever.py";
    static {
        System.setProperty("java.awt.headless", "false");
    }
    public static void main(String[] args) throws IOException {
        /**
         * 初始化桌面
         * 初始化输入
         * 加载资源
         *  加载背景
         *  加载角色
         *  加载道具
         *  加载声音
         */
        // TODO: 2020/5/11 servlet的架构图diagram
//        Process exec = Runtime.getRuntime().exec(CMD);
//        log.info("pythonServer启动"+exec.isAlive());
        String absolutePath = new File("").getAbsolutePath();
        log.debug(absolutePath);
        ConfigurableApplicationContext run = SpringApplication.run(PanguApplication.class, args);
        log.info("盘古启动成功");
        Data data = run.getBean(Data.class);
        log.info("数据总计:"+data.getWorldData().size());
        // soutv
        System.out.println("log = " + log);
    }

}
