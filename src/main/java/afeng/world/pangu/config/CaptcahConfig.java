package afeng.world.pangu.config;

import com.google.code.kaptcha.servlet.KaptchaServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @datetime: 2020/5/29 23:44
 * @project_name: pangu
 * @author: afeng
 */
@Configuration
public class CaptcahConfig {
    @Bean
    public ServletRegistrationBean servletBean() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean();
        registrationBean.addUrlMappings("/captcha");
        registrationBean.setServlet(new KaptchaServlet());
        return registrationBean;
    }
}
