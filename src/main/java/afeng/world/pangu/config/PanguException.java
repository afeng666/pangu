package afeng.world.pangu.config;

import afeng.world.pangu.common.enums.ResultCode;

/**
 * @description:
 * @author: liqf
 * @create: 2021-06-30 16:53
 **/
public class PanguException extends RuntimeException {

    /**
     * 异常码
     */
    private Integer code;

    /**
     * 异常提示信息
     */
    private String message;


    public PanguException(String message) {
        super(message);
        this.message = message;
    }

    /**
     * 异常错误信息
     *
     * @param resultCode
     */
    public PanguException(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMsg();
    }

    public PanguException(Throwable cause) {
        super(cause);
    }

    public PanguException(String message, Throwable cause) {
        super(message, cause);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

