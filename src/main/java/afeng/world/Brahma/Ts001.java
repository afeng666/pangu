package afeng.world.Brahma;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;

/**
 * @description: json的使用
 * @author: liqf
 * @create: 2023-01-17 21:05
 **/
public class Ts001 {
    public static void main(String[] args) {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("yes", "yes");
        objectObjectHashMap.put("yes1", "yes");
        objectObjectHashMap.put("yes2", "yes");
        objectObjectHashMap.put("yes3", "yes");
        String toJSONString = JSON.toJSONString(objectObjectHashMap);
        System.out.println("toJSONString = " + toJSONString);
    }
}

