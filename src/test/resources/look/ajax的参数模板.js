最小模板:
$.ajax({
    type:"", //请求类型
    url:"", //请求url
    data:{
        '':,
        '':
    }, //传参
    dataType:"", //返回的类型, 即res的类型
    success: function(res){
        //res返回的数据
    }
})

参数说明:
$.ajax({
    type: "GET",   //请求方式  get post  注意：其它 HTTP 请求方法，如 PUT 和 DELETE 也可以使用，但仅部分浏览器支持。
    url: "http://www.hzhuti.com",    //请求的url地址
    async: true, //请求是否异步，默认为异步，这也是ajax重要特性
    contentType: "application/json", //JQuery Ajax 以 application/json 上传 JSON字符串，后端用 @RquestBody 获取参数。
    data: { "id": "value" },    //参数值
    dataType: "json",   //返回格式为json
    beforeSend: function(request) {
      //请求前的处理
      request.setRequestHeader("Content-type","application/json");
      request.setRequestHeader("Source","101");
      request.setRequestHeader("Token","aaw--wssw-ss...");
    },
    success: function(data) {       //data返回的数据, 本项目是JsonResult
    //请求成功时处理
    },
    complete: function() {
      //请求完成的处理
    },
    error: function() {
      //请求出错处理
    }
});

------

data
Object,String
发送到服务器的数据。将自动转换为请求字符串格式。GET 请求中将附加在 URL 后。
查看 processData 选项说明以禁止此自动转换。必须为 Key/Value 格式。
如果为数组，jQuery 将自动为不同值对应同一个名称。
如 {foo:["bar1", "bar2"]} 转换为 '&foo=bar1&foo=bar2'。

dataType
String
预期服务器返回的数据类型。如果不指定，jQuery 将自动根据 HTTP 包 MIME 信息返回 responseXML 或 responseText，并作为回调函数参数传递，可用值:
"xml": 返回 XML 文档，可用 jQuery 处理。
"html": 返回纯文本 HTML 信息；包含 script 元素。
"script": 返回纯文本 JavaScript 代码。不会自动缓存结果。
"json": 返回 JSON 数据 。
"jsonp": JSONP 格式。使用 JSONP 形式调用函数时，
如 "myurl?callback=?" jQuery 将自动替换 ? 为正确的函数名，以执行回调函数。

error
Function
(默认: 自动判断 (xml 或 html)) 请求失败时将调用此方法。
这个方法有三个参数：XMLHttpRequest 对象，错误信息，（可能）捕获的错误对象。
function (XMLHttpRequest, textStatus, errorThrown) {
   // 通常情况下textStatus和errorThown只有其中一个有值  this;
}

timeout
Number
设置请求超时时间（毫秒）。此设置将覆盖全局设置。

cache
Boolean
(默认: true) jQuery 1.2 新功能，设置为 false 将不会从浏览器缓存中加载请求信息。

global
Boolean
(默认: true) 是否触发全局 AJAX 事件。
设置为 false 将不会触发全局 AJAX 事件，如 ajaxStart 或 ajaxStop 。
可用于控制不同的Ajax事件

ifModified
Boolean
(默认: false) 仅在服务器数据改变时获取新数据。使用 HTTP 包 Last-Modified 头信息判断。

processData
Boolean
(默认: true) 默认情况下，发送的数据将被转换为对象(技术上讲并非字符串) 以配合默认内容类型 "application/x-www-form-urlencoded"。
如果要发送 DOM 树信息或其它不希望转换的信息，请设置为 false。

success
Function
请求成功后回调函数。这个方法有两个参数：服务器返回数据，返回状态function (data, textStatus) {
  // data could be xmlDoc, jsonObj, html, text, etc...
}