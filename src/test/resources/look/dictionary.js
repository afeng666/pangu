layui.use(['table', 'exbase', 'LayerForm', 'LayerAjax'], function () {
    var table = layui.table;
    var exbase = layui.exbase;
    var $ = layui.$;

    var dictTable = table.render({
        elem: '#dict_table_id'
        , height: 'full-75'
        , id: 'dict_table_id'
        , url: function () {
            var url = ctxPath + '/admin/dictionaries/page'
            var keyword = $("#keyword_input_id").val()
            return url + "?keyword=" + keyword;
        }()
        , page: {
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip']
            , first: false //不显示首页
            , last: false //不显示尾页
        } //开启分页
        , cols: [[ //表头
            {checkbox: true}
            , {field: 'id', title: 'ID', width: 80, sort: true, hide: true}
            , {field: 'typeName', title: '字典名称'}
            , {field: 'type', title: '字典类型'}
            , {field: 'codeName', title: '字典值'}
            , {field: 'code', title: '字典编码'}
            , {field: 'ordered', title: '顺序'}
            , {field: 'createTime', title: '创建时间'}
            , {field: 'updateTime', title: '修改时间'}
        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalElements, //解析数据长度
                "data": res.data.content //解析数据列表
            };
        }
    });

    $("#add_button_id").on('click', function () {
        var form = new layui.LayerForm({
            url: ctxPath + "/admin/dictionaries",
            method: 'PUT',
            title: '创建字典',
            contentType: 'application/json',
            success: function () {
                dictTable.reload();
            },
            form: [{
                name: "typeName",
                label: "字典名称",
                type: 'text',
                verify: 'required'
            }, {
                name: "type",
                label: "字典编码",
                type: 'text',
                verify: 'required'
            }, {
                name: "codeName",
                label: "字典值",
                type: 'text',
                verify: 'required'
            }, {
                name: "code",
                label: "值编码",
                type: 'text',
                verify: 'required'
            }, {
                name: "ordered",
                label: '顺序',
                type: 'text',
                verify: 'required | number',
                value: 1
            }]
        })
        form.open()
    })
    $("#update_button_id").on('click', function () {
        var checkStatus = table.checkStatus('dict_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        if (checkStatus.data.length > 1) {
            layer.alert("仅支持选中一条数据操作", {title: '提示'})
            return
        }
        var dictId = checkStatus.data[0].id

        var form = new layui.LayerForm({
            url: ctxPath + "/admin/dictionaries/" + dictId,
            method: 'POST',
            title: '修改字典',
            contentType: 'application/json',
            success: function () {
                dictTable.reload();
            },
            onload: {
                url: ctxPath + "/admin/dictionaries/" + dictId,
                method: 'get'
            },
            form: [{
                name: "typeName",
                label: "字典名称",
                type: 'text',
                verify: 'required'
            }, {
                name: "type",
                label: "字典编码",
                type: 'text',
                verify: 'required'
            }, {
                name: "codeName",
                label: "字典值",
                type: 'text',
                verify: 'required'
            }, {
                name: "code",
                label: "值编码",
                type: 'text',
                verify: 'required'
            }, {
                name: "ordered",
                label: '顺序',
                type: 'text',
                verify: 'required | number',
                value: 1
            }, {
                name: 'id',
                label: 'id',
                type: 'hide',
                value: dictId
            }]
        })
        form.open()
    })
    $("#delete_button_id").on('click', function () {
        var checkStatus = table.checkStatus('dict_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id).join(",")

        var ajax = new layui.LayerAjax({
            title: "删除选中字典",
            url: ctxPath + "/admin/dictionaries/" + idList,
            method: "delete",
            success: function () {
                dictTable.reload();
            }
        })
        ajax.open();
    })

    layui.form.on('submit(search_button_filter)', function (data) {
        dictTable.reload({
            url: ctxPath + '/admin/dictionaries/page?keyword=' + (data.field['keyword'] || '')
        });
        return false;
    })
})