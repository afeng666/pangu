layui.use(['table', 'LayerForm', 'LayerAjax'], function () {
    var table = layui.table;
    var exbase = layui.exbase;
    var $ = layui.$;

    var userTable = table.render({
        elem: '#user_table_id'
        , height: 'full-60'
        , id: 'user_table_id'
        , url: ctxPath + '/admin/users/page' //数据接口
        , page: {
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip']
            , first: false //不显示首页
            , last: false //不显示尾页
        } //开启分页
        , cols: [[ //表头
            {checkbox: true}
            , {field: 'number', title: '', type: 'numbers'}
            , {field: 'id', title: 'ID', width: 80, sort: true, hide: true}
            , {field: 'account', title: '账号'}
            , {field: 'name', title: '用户名'}
            , {field: 'age', title: '年龄'}
            , {field: 'mobilePhone', title: '手机号'}
            , {field: 'state', title: '状态', templet: '#stateTemplate'}
            , {field: 'createTime', title: '创建时间'}
            , {field: 'updateTime', title: '修改时间'}
        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalElements, //解析数据长度
                "data": res.data.content //解析数据列表
            };
        }, done:function () {
            layui.form.on('switch', function(data){
                var $elem = $(data.elem)
                var id = $elem.attr("data-id")
                if(!id) {
                    return;
                }
                var checked = data.elem.checked ? true : false;
                var state = data.elem.checked ? "enabled" : "disabled";
                $.ajax({
                    url: ctxPath + '/admin/users/' + id + '/state',
                    method: 'post',
                    data: {state: state},
                    success: function (data) {
                        if (data.code != exbase.AJAX_SUCCESS_CODE) {
                            layer.msg('修改用户状态失败：' + data.msg, {icon: 2})
                            $elem.prop('checked', !checked)
                            layui.form.render('checkbox')
                        }
                    }
                })
            })
        }
    });

    $("#add_button_id").on('click', function () {
        var form = new layui.LayerForm({
            url: ctxPath + "/admin/users",
            method: 'PUT',
            title: '创建用户',
            contentType: 'application/json',
            success: function () {
                userTable.reload();
            },
            form: [{
                name: "account",
                label: "账号",
                type: 'text',
                verify: 'required'
            }, {
                name: "name",
                label: "姓名",
                type: 'text',
                verify: 'required'
            }, {
                name: "age",
                label: "年龄",
                type: 'text',
                verify: 'number_null'
            }, {
                name: "mobilePhone",
                label: "手机号",
                type: 'text',
                verify: 'phone_null'
            }, {
                name: "orgIds",
                label: '组织',
                type: 'select-tree',
                config: {
                    url: ctxPath + '/admin/orgs/-1/tree',
                    multiple: true,
                    format: function (data) {
                        data.title = data.name;
                        data.spread = true;
                        return data;
                    }
                },
                verify: 'required'
            }, {
                name: 'roleIds',
                label: '角色',
                type: 'select',
                multiple: true,
                dict_url: ctxPath + '/admin/roles/dict?scope=system'
            }, {
                name: "state",
                label: "状态",
                type: 'checkbox',
                value: 'enabled'
            }]
        })
        form.open()
    })

    $("#update_button_id").on('click', function () {
        var checkStatus = table.checkStatus('user_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        if (checkStatus.data.length > 1) {
            layer.alert("仅支持选中一条数据操作", {title: '提示'})
            return
        }
        var userId =  checkStatus.data[0].id
        var form = new layui.LayerForm({
            url: ctxPath + "/admin/users/" + userId,
            method: 'POST',
            title: '修改用户',
            contentType: 'application/json',
            onload: {
                url: ctxPath + "/admin/users/" + userId,
                method: 'get'
            },
            success: function () {
                userTable.reload();
            },
            form: [{
                name: "account",
                label: "账号",
                type: 'text',
                verify: 'required'
            }, {
                name: "name",
                label: "姓名",
                type: 'text',
                verify: 'required'
            }, {
                name: "age",
                label: "年龄",
                type: 'text',
                verify: 'number_null'
            }, {
                name: "mobilePhone",
                label: "手机号",
                type: 'text',
                verify: 'phone_null'
            }, {
                name: "state",
                label: "状态",
                type: 'checkbox',
                value: 'enabled'
            }, {
                name: 'id',
                label: 'id',
                type: 'hide'
            }]
        })
        form.open()

    })

    $("#delete_button_id").on('click', function () {
        var checkStatus = table.checkStatus('user_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id).join(",")

        var ajax = new layui.LayerAjax({
            title: "删除选中用户",
            url: ctxPath + "/admin/users/" + idList,
            method: "delete",
            success: function () {
                layer.msg("操作成功")
                userTable.reload();
            }
        })
        ajax.open();
    })

    $("#reset_pwd_button_id").on('click', function () {
        var checkStatus = table.checkStatus('user_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id).join(",")

        var ajax = new layui.LayerAjax({
            title: "重置用户密码",
            url: ctxPath + "/admin/users/" + idList + "/password/reset",
            method: "POST",
            success: function () {
                layer.msg("操作成功")
            }
        })
        ajax.open();
    })

})