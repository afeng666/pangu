layui.use(['table', 'exbase', 'form', 'layer', 'LayerAjax'], function () {
    var $ = layui.$
    var url = layui.url();
    var roleId = url.search.roleId
    var table = layui.table
    var exbase = layui.exbase

    var roleUserTable = table.render({
        elem: '#roleUser_table_id'
        , id: 'roleUser_table_id'
        , url: function () {
            var url = ctxPath + '/admin/roles/' + roleId + '/users/page'
            var keyword = $("#keyword_roleUser_input_id").val()
            return url + "?keyword=" + keyword;
        }()
        , page: {
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip']
            , first: false //不显示首页
            , last: false //不显示尾页
        } //开启分页
        , cols: [[ //表头
            {checkbox: true}
            , {field: 'id', title: 'ID', width: 80, sort: true, hide: true}
            , {field: 'account', title: '账号'}
            , {field: 'name', title: '用户名'}
            , {field: 'mobilePhone', title: '手机号'}
        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalElements, //解析数据长度
                "data": res.data.content //解析数据列表
            };
        }
    })

    var addRoleUserTable = table.render({
        title: '选择用户添加到当前角色',
        elem: '#add_roleUser_table_id'
        , id: 'add_roleUser_table_id'
        , url: ctxPath + '/admin/users/page?notRoleId=' + roleId
        , page: {
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip']
            , first: false //不显示首页
            , last: false //不显示尾页
        } //开启分页
        , cols: [[ //表头
            {checkbox: true}
            , {field: 'id', title: 'ID', width: 80, sort: true, hide: true}
            , {field: 'account', title: '账号'}
            , {field: 'name', title: '用户名'}
            , {field: 'mobilePhone', title: '手机号'}
        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalElements, //解析数据长度
                "data": res.data.content //解析数据列表
            };
        }
    })

    $("#add_roleUser_button_id").on('click', function () {
        addRoleUserTable.reload({
            url : ctxPath + '/admin/users/page',
            where: {
                notRoleId: roleId,
                keyword: ''
            }
        })
        $("#keyword_notRoleUser_input_id").val("")
        $("#roleUser_div_id").hide()
        $("#add_roleUser_div_id").show()
    })

    $("#return_roleUser_button_id").on('click', function () {
        $("#roleUser_div_id").show()
        $("#add_roleUser_div_id").hide()
    })

    $("#delete_roleUser_button_id").on('click', function () {
        var checkStatus = table.checkStatus('roleUser_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id).join(",")

        var ajax = new layui.LayerAjax({
            title: "移除这些用户的角色关联",
            url: ctxPath + "/admin/roles/" + roleId + "/users/" + idList,
            method: "delete",
            success: function (data) {
                if(data.code == exbase.AJAX_SUCCESS_CODE) {
                    layer.msg("移除用户角色关联成功")
                    roleUserTable.reload();
                }else{
                    layer.msg(data.msg, {icon: 2})
                }
            }
        })
        ajax.open();
    })

    $("#submit_roleUser_button_id").on('click',function (){
        var checkStatus = table.checkStatus('add_roleUser_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id)
        $.ajax({
            url: ctxPath + "/admin/roles/" + roleId + "/users",
            method: 'POST',
            data: {userIds: idList},
            success:function (data) {
                if(data.code == exbase.AJAX_SUCCESS_CODE) {
                    layer.msg("添加用户到当前角色成功")
                    $("#return_roleUser_button_id").click()
                    roleUserTable.reload()
                } else {
                    layer.msg(data.msg, {icon: 2})
                }
            }
        })
    })

    layui.form.on('submit(search_roleUser_button_filter)', function (data) {
        roleUserTable.reload({
            url: ctxPath + '/admin/roles/' + roleId + '/users/page?keyword=' + (data.field['keyword'] || '')
        })
        return false
    })

    layui.form.on('submit(search_notRoleUser_button_filter)', function (data) {
        addRoleUserTable.reload({
            url: ctxPath + '/admin/users/page',
            where: {
                notRoleId: roleId,
                keyword: (data.field['keyword'] || '')
            }
        })
        return false;
    })


})