layui.use(['ExTree', 'DynamicForm', 'LayerAjax', 'LayerForm'], function () {
    var $ = layui.$;
    var Menu = {
        treeLoader: function () {
            return new layui.ExTree({
                id: "menu_tree",
                elem: '#menu_tree_id',
                url: ctxPath + "/admin/menus/-1/tree?recursive=true",
                onlyIconControl: true,
                format: function (data) {
                    data.title = data.name;
                    data.spread = true;
                    return data;
                },
                rightMenus: function (node) {
                    if (node.data.leaf) {
                        return [{
                            name: "删除",
                            click: function (node) {
                                Menu.configDeleteButton(node);
                            }
                        }];
                    } else if (node.data.parentId == -1) {
                        return [{
                            name: "新增子菜单",
                            click: function (node) {
                                Menu.configAddButton(node);
                            }
                        }]
                    } else {
                        return [{
                            name: "新增子菜单",
                            click: function (node) {
                                Menu.configAddButton(node);
                            }
                        }, {
                            name: "删除",
                            click: function (node) {
                                Menu.configDeleteButton(node);
                            }
                        }];
                    }
                },
                click: function (node) {
                    if (node.data.parentId == -1) {
                        $("#edit_form_div").empty();
                    } else {
                        var form = new layui.DynamicForm({
                            elem: '#edit_form_div',
                            title: '编辑菜单',
                            url: ctxPath + "/admin/menus",
                            method: "POST",
                            contentType: "application/json",
                            onload: {
                                url: ctxPath + "/admin/menus/" + node.data.id,
                                method: "GET",
                                contentType: "application/json"
                            },
                            success: function (data) {
                                Menu.treeLoader.load();
                            },
                            form: [{
                                label: "名称",
                                name: "name",
                                type: "text"
                            }, {
                                label: "图标",
                                name: "icon",
                                type: "text"
                            }, {
                                label: "链接",
                                name: "link",
                                type: "text"
                            }, {
                                label: "排序",
                                name: "ordered",
                                type: "text"
                            }, {
                                label: "叶子节点",
                                name: "leaf",
                                type: "radio",
                                options: [{
                                    label: '是',
                                    value: true
                                }, {
                                    label: "否",
                                    value: false
                                }]
                            }, {
                                label: "parentId",
                                name: "parentId",
                                type: "hide"
                            }, {
                                label: "id",
                                name: "id",
                                type: "hide"
                            }]
                        });
                        form.render();
                    }
                }
            })
        }(),

        init: function () {
            Menu.treeLoader.load();
        },
        configAddButton: function (node) {
            var form = new layui.LayerForm({
                title: "创建菜单",
                form: [{
                    label: "名称",
                    name: "name",
                    type: "text"
                }, {
                    label: "图标",
                    name: "icon",
                    type: "text"
                }, {
                    label: "链接",
                    name: "link",
                    type: "text"
                }, {
                    label: "排序",
                    name: "ordered",
                    type: "text"
                }, {
                    label: "叶子节点",
                    name: "leaf",
                    type: "radio",
                    options: [{
                        label: '是',
                        value: true
                    }, {
                        label: "否",
                        value: false,
                        checked: "checked"
                    }]
                }, {
                    label: "parentId",
                    name: "parentId",
                    type: "hide",
                    value: node.data.id
                }],
                url: ctxPath + "/admin/menus",
                method: "PUT",
                contentType: "application/json",
                success: function (data) {
                    Menu.treeLoader.load();
                }
            });
            form.open();
        },
        configDeleteButton: function (node) {
            var dialog = new layui.LayerAjax({
                url: ctxPath + "/admin/menus/" + node.data.id,
                method: "DELETE",
                success: function (data) {
                    Menu.treeLoader.load();
                    layer.alert("删除成功", {icon: 1})

                    var idInput = $("#edit_form_div").find("input[name=id]")
                    if (idInput && idInput.val() == node.data.id) {
                        $("#edit_form_div").empty()
                    }
                }
            })
            dialog.open();
        }
    }

    Menu.init()

})