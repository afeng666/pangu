var laydate = layui.laydate;   // 时间
var table = layui.table;    //表格
var form = layui.form;  //表单
var laypage = layui.laypage; //分页

//执行一个laydate实例
laydate.render({
    elem: '#start' //指定元素
});
//执行一个laydate实例
laydate.render({
    elem: '#end' //指定元素
});

var url ="xxxxxx.xxxx.xxx";
var tableName = table.render({
    elem: '#demo',   //table的id
    url: url,
    method: 'get', //get post
    contentType: "application/json",
    page: true,  //开启分页
    request: {
        pageName: 'pageIndex' //页码的参数名称，默认：page，更改之后为pageIndex，在请求时会自动带过去
        , limitName: 'pageSize' //每页数据量的参数名，默认：limit，更改之后为pageSize
    },
    parseData: function (res) {  //与后端数据交互，需要传固定格式，转换为下面所示
        return {
            "code": 0, //数据状态
            "msg": "",  //状态信息
            "count": res.Data.Count, //数据总数
            "data": res.Data.Dtos, //后端的详细数据
        }
    },
    cols: [//表头
        [{ field: 'column1', title: '列1', align: 'center', width: 240 },
        { field: 'column2', title: '列2', align: 'center' },
        { field: 'column3', title: '列3', align: 'center' },
        ]],
    loading: true, //数据加载中。。。
    id: 'idTest',
});

//搜索   按钮在form表单中 样式class="layui-btn"  lay-submit lay-filter="search"
 //form内的按钮总会自动提交，将其设置为button，不会自动提交
 // button 按钮的type有三个属性：button，submit，reset，button默认是submit，所以没有指定type类型情况下，点击button会触发form表单
 //按钮需要加上lay-submit属性，layut的form.on的表单提交监听不到这个按钮，那么return false对提交的制止也就失效了
form.on('submit(search)',
    function (data) {
        var param1= $("#start").val();
        var param2= $("#end").val();
        var url =  "xxxxxxx?param1=" + param + "&param2=" + param2 ; //第一种方法
        tableName.reload({
            url: url
            ,request: {
                pageName: 'pageIndex' //页码的参数名称，默认：page
                , limitName: 'pageSize' //每页数据量的参数名，默认：limit

            }
            ,where: { //设定异步数据接口的额外参数，任意设 第二种方法传参
                parameter1: param1,
                parameter2: param2,
            }
            , page: {
                curr: 1 //重新从第 1 页开始
            }
        });
        return false;   //表单按钮的js回调函数添加retrun false制止
    });
 //<button  id="reloadbtn" > jquery 写法
//$('#reloadbtn').click(function () {
    //里面写法和上面一样
//});

});

// 监听工具条
table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
                if (layEvent === 'detail') { //查看
                    var id = data["Id"];
                    xadmin.open('名字', 'url?Id=' + id);
                }
                else if (layEvent === 'del') { //删除
                    layer.confirm('真的删除行么', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        //向服务端发送删除指令
                    });
                }
                else if (layEvent === 'edit') { //编辑
                    var column1= data["column1"]; //编辑行的column1的值
                    var column2= data["column2"];
                    xadmin.open('编辑', 'url?column1=' + column1+ '&column2=' + column2);
                }

            });
        });


// 数据动态加载,使用laytpl
layui.use(['laytpl'], function () {
        var laytpl = layui.laytpl;
        var Id = getQueryVariable("Id");
        var host = "https://localhost:5001";
        layui.use(['laytpl'],
            function () {
                var laytpl = layui.laytpl;var url = url?id=" + Id;
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success: function (res) {  //res请求返回的结果for (i = 0; i < res.Data.length;i++) {
                            $("#test").append("<ul class='layui-timeline'><li class='layui-timeline-item'> <i class='layui-icon layui-timeline-axis'>&#xe63f;</i>                        <div class='layui-timeline-content layui-text'><h3 class='layui-timeline-title'>" + res.Data[i].xxx1+ "</h3><p><br>" + res.Data[i].xxx2+ "</p></li></ul>");
                        }
                    }
                })

            });
    });

   <script type="text/html" >
       <div class="layui-card">
           <div class="layui-card-header">订单概况</div>
           <div class="layui-card-body" id="test">
           </div>
       </div>
   </script>

 // 监听提交
 //监听提交
         form.on('submit(commit)', function (data) {
             var column1= data.field.column1;
             var column2= data.field.column2;
             var list= [];  //这里数组要与后台的数组中的对象要对应，不然的话接受不到数据
             $('input[type=checkbox]:checked').each(function () {  //获取checkbox选择的属性
                 var title = this.title;  //获取标题                var value=$(this).val();
                 var list= { Title:title, Value: value };
                 courseList.push(courseDto);
             });
             var courseManagementDtos = { Column1: column1, Column2: column2, List: list };  //注意名字要对应
             var url ="后台接口地址";
             $.ajax({
                 url: url,
                 type: "post",
                 contentType: "application/json",
                 dataType: "json",
                 data: JSON.stringify(courseManagementDtos),
                 success: function (res) {
                     if (res.Code == 1) {
                         layer.alert("提交成功");
                     }
                     else {
                         layer.alert("提交失败");
                     }
                 }
             })

             return false;
         });