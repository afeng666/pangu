function openUpdateDialog(roleId, roleTable) {
    var form = new layui.LayerForm({
        url: ctxPath + "/admin/roles/" + roleId,
        method: 'POST',
        title: '修改角色',
        contentType: 'application/json',
        onload: {
            url: ctxPath + "/admin/roles/" + roleId,
            method: 'get'
        },
        success: function () {
            roleTable.reload();
        },
        form: [{
            name: "name",
            label: "角色名称",
            type: 'text',
            verify: 'required'
        }, {
            name: "code",
            label: "角色编码",
            type: 'text',
            verify: 'required'
        }, {
            name: "scope",
            label: "角色类型",
            type: 'select',
            dict_url: ctxPath + "/admin/dictionaries?type=roleType",
            verify: 'required'
        }, {
            name: "enabled",
            label: "状态",
            type: 'radio',
            options: [{
                label: '启用',
                value: true
            }, {
                label: '禁用',
                value: false
            }]
        }, {
            name: "description",
            label: '描述',
            type: 'text'
        }]
    })
    form.open()
}

function openDeleteDialog(idList, roleTable) {
    var ajax = new layui.LayerAjax({
        title: "删除选中角色",
        url: ctxPath + "/admin/roles/" + idList,
        method: "delete",
        success: function () {
            roleTable.reload();
        }
    })
    ajax.open();
}

layui.use(['table', 'exbase', 'LayerForm', 'LayerAjax'], function () {
    var table = layui.table;
    var exbase = layui.exbase;
    var $ = layui.$;

    var roleTable = table.render({
        elem: '#role_table_id'
        , height: 'full-75'
        , id: 'role_table_id'
        , url: function () {
            var url = ctxPath + '/admin/roles/page'
            var keyword = $("#keyword_input_id").val()
            return url + "?keyword=" + keyword;
        }()
        , page: {
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip']
            , first: false //不显示首页
            , last: false //不显示尾页
        } //开启分页
        , cols: [[ //表头
            {checkbox: true}
            , {field: 'id', title: 'ID', width: 80, sort: true, hide: true}
            , {field: 'name', title: '角色名称'}
            , {field: 'code', title: '角色编码'}
            , {field: 'scope', title: '角色类型'}
            , {field: 'enabled', title: '状态', templet: '#enabledTemplate'}
            , {field: 'description', title: '描述'}
            , {field: 'createTime', title: '创建时间'}
            , {field: 'updateTime', title: '修改时间'}
            , {field: '', title: '操作类型', templet: '#opTemplate'}
        ]]
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalElements, //解析数据长度
                "data": res.data.content //解析数据列表
            };
        }, done: function () {
            layui.form.on('switch', function(data){
                var $elem = $(data.elem)
                var id = $elem.attr("data-id")
                if(!id) {
                    return;
                }
                var checked = data.elem.checked ? true : false;
                $.ajax({
                    url: ctxPath + '/admin/roles/' + id + '/enabled',
                    method: 'post',
                    data: {enabled: checked},
                    success: function (data) {
                        if (data.code != exbase.AJAX_SUCCESS_CODE) {
                            layer.msg('修改角色状态失败：' + data.msg, {icon: 2})
                            $elem.prop('checked', !checked)
                            layui.form.render('checkbox')
                        }
                    }
                })
            })

            $("button.update-role-class").on('click', function () {
                var $this = $(this)
                var id = $this.attr('data-id')
                openUpdateDialog(id, roleTable)
            })

            $("button.delete-role-class").on('click', function () {
                var $this = $(this)
                var id = $this.attr('data-id')
                openDeleteDialog(id, roleTable)
            })

            $("button.manager-roleUser-class").on('click',function () {
                var $this = $(this)
                var roleId = $this.attr('data-id')
                var roleName = $this.attr('data-name')
                layer.open({
                    type: 2,
                    title: roleName + '：用户列表',
                    shadeClose: true,
                    shade: 0.4,
                    area: ['600px', '400px'],
                    content: ctxPath + '/view/admin/roleUsers?roleId=' + roleId //iframe的url
                });
            })

            $("button.manager-roleMenu-class").on('click',function () {
                var $this = $(this)
                var roleId = $this.attr('data-id')
                var roleName = $this.attr('data-name')
                layer.open({
                    type: 2,
                    title: roleName + '：菜单权限',
                    shadeClose: true,
                    shade: 0.4,
                    area: ['400px', '400px'],
                    content: ctxPath + '/view/admin/roleMenus?roleId=' + roleId //iframe的url
                });
            })
        }
    });

    $("#add_button_id").on('click', function () {
        var form = new layui.LayerForm({
            url: ctxPath + "/admin/roles",
            method: 'PUT',
            title: '创建角色',
            contentType: 'application/json',
            success: function () {
                roleTable.reload();
            },
            form: [{
                name: "name",
                label: "角色名称",
                type: 'text',
                verify: 'required'
            }, {
                name: "code",
                label: "角色编码",
                type: 'text',
                verify: 'required'
            }, {
                name: "scope",
                label: "角色类型",
                type: 'select',
                dict_url: ctxPath + "/admin/dictionaries?type=roleType",
                verify: 'required'
            }, {
                name: "enabled",
                label: "状态",
                value: 'true',
                type: 'radio',
                options: [{
                    label: '启用',
                    value: true,
                    checked: true
                }, {
                    label: '禁用',
                    value: false
                }]
            }, {
                name: "description",
                label: '描述',
                type: 'text'
            }]
        })
        form.open()
    })
    $("#update_button_id").on('click', function () {
        var checkStatus = table.checkStatus('role_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        if (checkStatus.data.length > 1) {
            layer.alert("仅支持选中一条数据操作", {title: '提示'})
            return
        }
        var roleId = checkStatus.data[0].id

        openUpdateDialog(roleId, roleTable);
    })
    $("#delete_button_id").on('click', function () {
        var checkStatus = table.checkStatus('role_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请至少选中一条数据后操作", {title: '提示'})
            return
        }
        var idList = checkStatus.data.map(a => a.id).join(",")

        openDeleteDialog(idList, roleTable);
    })

    layui.form.on('submit(search_button_filter)', function (data) {
        roleTable.reload({
            url: ctxPath + '/admin/roles/page?keyword=' + (data.field['keyword'] || '')
        });
        return false;
    })

    $('#users_button_id').on('click', function () {
        var checkStatus = table.checkStatus('role_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        if (checkStatus.data.length > 1) {
            layer.alert("仅支持选中一条数据操作", {title: '提示'})
            return
        }
        var roleId = checkStatus.data[0].id
        var roleName = checkStatus.data[0].name

        layer.open({
            type: 2,
            title: roleName + '：用户列表',
            shadeClose: true,
            shade: 0.4,
            area: ['600px', '400px'],
            content: ctxPath + '/view/admin/roleUsers?roleId=' + roleId //iframe的url
        });
    })


    $('#menus_button_id').on('click', function () {
        var checkStatus = table.checkStatus('role_table_id');
        if (checkStatus.data.length == 0) {
            layer.alert("请选中一条数据后操作", {title: '提示'})
            return
        }
        if (checkStatus.data.length > 1) {
            layer.alert("仅支持选中一条数据操作", {title: '提示'})
            return
        }
        var roleId = checkStatus.data[0].id
        var roleName = checkStatus.data[0].name

        layer.open({
            type: 2,
            title: roleName + '：菜单权限',
            shadeClose: true,
            shade: 0.4,
            area: ['400px', '400px'],
            content: ctxPath + '/view/admin/roleMenus?roleId=' + roleId //iframe的url
        });
    })
})