package afeng.world.pangu.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

class MyHandlerInterceptorTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorProperties HealthIndicatorProperties;

    @Test
    void preHandle() {
    }

    @Test
    void postHandle() {
    }

    @Test
    void afterCompletion() {
    }

    @Test
    void myname() {

    }
}