package afeng.world.pangu.input;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @description:
 * @author: liqf
 * @create: 2021-09-27 16:03
 **/
public class JavaInput {

    @Test
    void boadInput1() {
        Scanner input = new Scanner(System.in);

        System.out.println("\"\" = " + "聊天输入:");
        while (input.hasNext()){
            String s1 = input.nextLine();
            System.out.println("s1 = " + s1);
        }
        input.close();

    }

    @SneakyThrows
    @Test
    void boadInput2() {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String s = input.readLine();

    }

    public static void main(String[] args) {
        new JavaInput().boadInput1();
        new JavaInput().boadInput2();
    }
}

