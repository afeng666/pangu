package afeng.world.pangu.泛型;

/**
 * @description: 泛型方法
 * @author: liqf
 * @create: 2021-08-30 11:54
 **/
public class FanMethod {

    public static <E> void printArray(E[] inputArray) {
        for (E element : inputArray) {
            System.out.printf("%s ", element);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // 创建不同类型数组： Integer, Double 和 Character
        Integer[] intArray = { 1, 2, 3 };
        String[] stringArray = { "Hello", "World" };
        printArray(intArray);
        printArray(stringArray);

    }
}

