package afeng.world.pangu.泛型;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @description: 泛型类
 * @author: liqf
 * @create: 2021-08-30 11:00
 **/
@Slf4j
@Data
//@ToString tostring的方法重写是拼接字符串
public class MyTclass<H>  {


    private H key;
    private H key1;
    private String key2;
    private int key3;

    public MyTclass(H key) {
        this.key = key;
    }

    public H getKey() {
        return key;
    }

//    @Override
//    public String toString() {
//        return (String) key;
//    }

    public static void main(String[] args) {
        System.out.println("new MyTclass<String>(\"222\") = " + new MyTclass<String>("222"));
    }
}

