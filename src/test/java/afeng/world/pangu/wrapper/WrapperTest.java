package afeng.world.pangu.wrapper;

import org.junit.jupiter.api.Test;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class WrapperTest {
    @Test
    void name() {

        // 技巧， tab可以快速修改后面的方法
        ByteBuffer buffer = ByteBuffer.allocate(10
        );

        //域外内存 （jvm世界外) DirectByteBuffer直接使用堆外内存， unsafe
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(100);
    }
}
