package afeng.world.pangu;

import afeng.world.pangu.service.OrderService;
import afeng.world.pangu.service.OrderServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @datetime: 2020/8/7 21:06
 * @project_name: pangu
 * @author: afeng
 */
public class ProxyDemo {

    public void test() {
        Object o = Proxy.newProxyInstance(getClass().getClassLoader(), OrderService.class.getInterfaces(), new InvocationHandler() {

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object invoke = method.invoke(new OrderServiceImpl(), args);
                return invoke;
            }
        });
    }
}
