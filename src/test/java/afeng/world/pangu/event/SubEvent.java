package afeng.world.pangu.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * @datetime: 2021/2/25 20:32
 * @project_name: pangu
 * @author: afeng
 */
public class SubEvent implements ApplicationListener {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("event.toString() = " + event.toString());
    }
}
