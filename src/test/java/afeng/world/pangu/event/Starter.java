package afeng.world.pangu.event;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @datetime: 2021/2/25 20:08
 * @project_name: pangu
 * @author: afeng
 */
public class Starter {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("/app.xml");

        Object hello = classPathXmlApplicationContext.getBean("hello");
        System.out.println("hello = " + hello);

        MyEvent myEvent = new MyEvent(new String("1111"));
//        PublishEvent publishEvent = (PublishEvent)classPathXmlApplicationContext.getBean("publishEvent");
        PublishEvent.publishEvent(myEvent);

    }
}
