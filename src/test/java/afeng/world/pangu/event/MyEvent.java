package afeng.world.pangu.event;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationEvent;

/**
 * @datetime: 2021/2/25 20:07
 * @project_name: pangu
 * @author: afeng
 */
public class MyEvent extends ApplicationEvent {
    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public MyEvent(Object source) {
        super(source);
    }

    @Test
    void name() {

    }
}
