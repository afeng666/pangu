package afeng.world.pangu.统计11位手机号码;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * @description:
 * @author: liqf
 * @create: 2021-08-19 19:55
 **/
public class NumberStatics {
    /**
     * 1 创建文件, 生成11位的电话号码
     * 2 统计相同的电话号码多少个
     */

    @SneakyThrows
    public File createFile(Integer num){
        Package aPackage = this.getClass().getPackage();
        System.out.println("aPackage = " + aPackage.toString());

        File file = new File(String.valueOf(System.currentTimeMillis()));
        if (file.exists()) {
            return file;
        }else {
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            // int的最大值 2^31-1 换算成十进制有10位
            Long a = 12345678901L;
//            Long b = 1L;

            //long转成字节🔢
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeLong(a);
//            dataOutputStream.writeLong(b);
            byte[] bytes = byteArrayOutputStream.toByteArray();

            fo.write(bytes);
        }
        return file;
    }

    public static void main(String[] args) {
        File file = new NumberStatics().createFile(11);
        long maxSize = (long) Math.pow(10,11);
        System.out.println("maxSize = " + maxSize);
        // bit [maxSize]
        // bit[a] =1
        // 数组求和
    }
}

