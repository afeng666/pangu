package afeng.world.pangu.syn;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @description:
 * @author: liqf
 * @create: 2021-09-02 09:22
 **/
public class SynStatus {
    public static void main(String[] args) {
//
//    }
//    @Test
//    void name() {

        Executor executor = Executors.newFixedThreadPool(8);

        Runnable runnable = new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                System.out.println("this = " + this);
                synchronized (this) {
                    System.out.println("executor = " + executor);
                    Thread.sleep(1000);
                }
            }
        };
        while (true) {
//            synchronized (this) {
                executor.execute(runnable);
//            }
        }

    }
}

