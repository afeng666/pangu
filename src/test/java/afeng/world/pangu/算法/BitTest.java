package afeng.world.pangu.算法;

import org.junit.jupiter.api.Test;

import java.util.Random;

public class BitTest {
    @Test
    void equal() {
        System.out.println("1<<34 = " + (1 << 34));
        System.out.println("1<<2 = " + (1 << 2));
    }

    /**
     * 判断相同位
     */
    @Test
    void nK() {
        int arr[] = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i+1;
            if (i == arr.length - 1) {
                arr[i] = new Random().nextInt(arr.length);
            }
        }
        for (int i : arr) {
            System.out.println("i = " + i);
        }

        int x = 0;
        for (int i : arr) {
            x = x^i;
            System.out.println("x = " + x);

        }
        for (int i = 0; i < arr.length; i++) {
            x = x^arr[i];
            System.out.println(" = " + x);
        }
    }

    @Test
    void testBase() {
        int a = 215;
        System.out.println(a & 1); // 00001  --> 0/1
        System.out.println(a ^ 1^1); // 00001  001^001=000^其他=其他 -- 1与1变化
        System.out.println(a | 1); // 1|1=1 1|0=1  | 0001 最终要换到二进制算
        byte b = (byte) 111;
        System.out.println(b & 1);
        System.out.println(b ^ 1);
        System.out.println(b | 1);
    }
}
