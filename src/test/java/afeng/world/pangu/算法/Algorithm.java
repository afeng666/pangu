package afeng.world.pangu.算法;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @datetime: 2020/9/30 13:02
 * @project_name: pangu
 * @author: afeng
 */
public class Algorithm {

    @Test
    void 贪心() {
        int[] con = new int[]{1, 10, 20, 50, 100, 200, 500};
        int target = 12345;
        int maxValueIndex = con.length - 1;
        List<Integer> out = new ArrayList();

        getHowCanPay(target, con, maxValueIndex, out);
        System.out.println("getHowCanPay(" + target + ") = " + out.toString());
    }

    private void getHowCanPay(int target, int[] con, int maxValueIndex, List<Integer> out) {
        int num = target / con[maxValueIndex];
        for (int i = 0; i < num; i++) {
            out.add(con[maxValueIndex]);
        }
        if (target % con[maxValueIndex] != 0) {
            getHowCanPay(target % con[maxValueIndex], con, maxValueIndex - 1, out);
        }
    }

    /**
     * 承载数据唯有byte与String
     * python有矩阵
     */
    @Test
    void 字节写文件() throws IOException {
        byte[] bytes = "生命诚可贵".getBytes();
        System.out.println("Arrays.toString(bytes) = " + Arrays.toString(bytes));
        FileOutputStream fos = new FileOutputStream("test.txt", true);
        fos.write(bytes);
        fos.close();
    }

    @Test
    void 相似度计算() {
        String str1 = "hello";
        String str2 = "hellohlll";

        int s1Len = str1.length();
        int s2Len = str2.length();

        char[] chars = new char[s1Len];
        char[] s2chars = new char[s2Len];
//        StringUtils.
        str1.getChars(0, s1Len, chars, 0);
        str2.getChars(0, s2Len, s2chars, 0);

        char[] filterChars = new char[s2Len];
        //与位置无关的相似度计算
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            for (int j = 0; j < s2chars.length; j++) {
                if (chars[i] == s2chars[j]) {
                    if (filterChars[j] == 0) {
                        count++;
                        filterChars[j] = 1;
                        break;
                    }
                }
            }
        }
        // TODO: 2020/10/22 位置相关, 最大似然?

        System.out.println("similar = " + (float)(count<<1)/(s1Len+s2Len));
    }
}
