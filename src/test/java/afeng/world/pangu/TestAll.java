package afeng.world.pangu;

import org.junit.jupiter.api.Test;

/**
 * 自动测试框架
 */
public class TestAll {

    /**
     * 测试一些包下的测试
     */
    public static void main(String[] args) {
        
    }

    @Test
    void 生成算法批量下载url() {
        /**
         * 生成b站 算法很美, 下载批量链接
         */
        for (int i = 1; i <= 234; i++) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("https://www.bilibili.com/video/BV1e7411T7FV?p=");
            stringBuffer.append(i);
            System.out.println(stringBuffer.toString());
            if (i % 20 == 0) {
                System.out.println();
            }
        }
    }
}
