package afeng.world.pangu;

import afeng.world.pangu.service.GameManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class PanguApplicationTests {

    @Autowired
    private GameManager gameManager;

    @Test
    void contextLoads() {
        log.info(gameManager.toString());
    }

}
