package afeng.world.pangu.exception;

import java.io.IOException;

/**
 * @description:
 * @author: liqf
 * @create: 2021-08-22 17:41
 **/
public class Ept {

    /**
     * Created by monkeysayhi on 2017/10/1.
     */
        private void fun1() throws IOException {
            throw new IOException("level 1 exception");
        }
        private void fun2() {
            try {
                fun1();
            } catch (IOException e) {
                throw new RuntimeException("level 2 exception", e);
            }
        }
        public static void main(String[] args) {
            try {
                new Ept().fun2();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}

