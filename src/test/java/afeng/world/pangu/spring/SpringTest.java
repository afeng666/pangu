package afeng.world.pangu.spring;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {
    @Test
    void testSpring() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        Object person = (Person) applicationContext.getBean("person");
        System.out.println("person = " + person);

        Person person1 = applicationContext.getBean("person", Person.class);
        System.out.println("person1 = " + person1);

        boolean person2 = applicationContext.containsBean("person");
        System.out.println("person2 = " + person2);

        boolean b = applicationContext.containsBeanDefinition("3p");
        System.out.println("b = " + b);
    }
}
