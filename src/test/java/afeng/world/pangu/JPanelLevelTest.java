package afeng.world.pangu;

import afeng.world.pangu.tools.PictureTools;

import javax.swing.*;
import javax.swing.plaf.PanelUI;
import java.awt.*;

public class JPanelLevelTest {

    public static void main(String[] args) throws AWTException {
        JFrame jFrame = new JFrame();
        jFrame.setSize(500, 500);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(null); //绝对定位
        jFrame.setVisible(true);

        Container contentPane = jFrame.getContentPane();
        contentPane.setBackground(Color.RED);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.setBackground(Color.BLUE);
        jPanel.setBounds(5, 5, contentPane.getWidth() - 10, contentPane.getHeight() - 10);
        contentPane.add(jPanel);

        JPanel childOne = new JPanel();
        childOne.setBounds(5, 5, jPanel.getWidth() - 10, jPanel.getHeight() - 10);
        childOne.setBackground(Color.BLACK);
        jPanel.add(childOne);

        JPanel childTwo = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_1.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_2.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_3.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_4.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_5.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_6.png"),30,30,this);
                g.drawImage(PictureTools.getImage("asset/img/actor/light_7.png"),30,30,this);
                removeAll();

            }

//            @Override
//            public void paint(Graphics g) {
//                super.paint(g);
//
//            }
        };
        childTwo.setBackground(Color.yellow);
        childTwo.setBounds(15, 15, jPanel.getWidth() - 30, jPanel.getHeight() - 30);
        jPanel.add(childTwo, null, 0); //0最外层

        Robot robot = new Robot();
        while (true) {
            robot.delay(1000);
//            childTwo.removeAll();
            childTwo.repaint();
            PanelUI ui = childTwo.getUI();

            jFrame.repaint();
            System.out.println(11);
        }

    }
}
