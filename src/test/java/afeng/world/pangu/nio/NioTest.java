package afeng.world.pangu.nio;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Set;

/**
 * 有事没事写点代码舒服!!!
 *
 * @datetime: 2020/12/1 21:58
 * @project_name: pangu
 * @author: afeng
 */
public class NioTest {
    @Test
    void InterBufferedTest() {

        IntBuffer intBuffer = IntBuffer.allocate(10);
        for (int i = 0; i < 10; i++) {
            int i1 = new SecureRandom().nextInt(20);
            intBuffer.put(i1);

        }
    }

    @Test
    void buffer() {

        ByteBuffer byteBuffer = ByteBuffer.allocate(10);

        ByteBuffer readOnlyBuffer = byteBuffer.asReadOnlyBuffer();

        byteBuffer.put((byte) 1);
        byteBuffer.flip();

        System.out.println("byteBuffer.get() = " + byteBuffer.get());
    }

    @Test
    void selectorTest() {

        try {
//            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
//            InetSocketAddress inetSocketAddress = new InetSocketAddress(8005);
//            serverSocketChannel.bind(inetSocketAddress);
//            SocketChannel accept = serverSocketChannel.accept();


            Selector selector = Selector.open();
            System.out.println("selector.isOpen() = " + selector.isOpen());
            Set<SelectionKey> keys = selector.keys();
            keys.forEach((key) -> {
                System.out.println("key = " + key);
                return; // 同for循环中continue
            });
            for (SelectionKey key : keys) {
                System.out.println("key = " + key);
                break;
            }

            SelectorProvider provider = selector.provider();
            DatagramChannel datagramChannel = provider.openDatagramChannel();
            SocketAddress socketAddress = new InetSocketAddress(8005);
            datagramChannel.bind(socketAddress);
            System.out.println("datagramChannel.isConnected() = " + datagramChannel.isConnected());

            Object object = new Object();

            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            selectionKeys.forEach((key) -> {
                System.out.println("object.hashCode() = " + object.hashCode());
                Selector selector1 = key.selector();
            });

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
}
