公开仓库
# nacos下载使用
Nacos的稳定版本下载地址https://github.com/alibaba/nacos/releases，下载最新版，本文下的是tag.gz文件，下载后解压即安装完成，然后进入解压目录后的bin目录执行如下命令启动Nacos。

    sh startup.sh -m standalone
# 配置文件
* 优先properties文件找, 没有再到yml   
* 优先config目录, 再resources下, 根据mvn激活相应的不同环境配置

# 依赖
* mysql
* nacos
* redis
# 启动步骤:
1. 启动nacos服务 [降级处理若是没有启动]
2. 确认nacos连接配置
3. 启动PanguApplication    

# 项目文档
https://www.yuque.com/ky3orc/gf9hhn/bvttsm
# 开发日志
https://www.yuque.com/ky3orc/gf9hhn/vhtnb4

# 开始集成相应的模块
1. layui
2. vue
3. 小程序, 前后端分离
4. 大全量系统
   

   

