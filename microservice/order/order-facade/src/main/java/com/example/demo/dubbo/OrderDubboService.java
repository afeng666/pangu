package com.example.demo.dubbo;

public interface OrderDubboService {
    void sayHello();

    String getDubboMsg(String req);
}
