package com.example.demoservices.nacos;

import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import com.alibaba.nacos.api.config.annotation.NacosProperty;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * @author afeng
 * @des
 * @date 2021/8/21 6:29 PM
 */
@EnableDiscoveryClient //开启服务注册发现
@Configuration
@NacosConfigurationProperties(dataId = "order.config")
public class NacosConfiguration {

}
