package com.example.demoservices.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: liqf
 * @create: 2021-09-28 17:31
 **/

@RestController
public class OrderController {

    @RequestMapping("order")
    public String order(){
        return "orderController";
    }
}

