package com.example.demoservices.dubbo;

import com.example.demo.dubbo.OrderDubboService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: liqf
 * @create: 2021-08-24 10:59
 **/
@DubboService
@Service
public class OrderDubboServiceImpl implements OrderDubboService {

    @Override
    public void sayHello(){
        System.out.println("true = " + true);
    }

    @Override
    public String getDubboMsg(String req) {
        return req;
    }
}

