package com.example.demo.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.example.demo.dubbo.OrderDubboService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author afeng
 * @des
 * @date 2021/8/21 6:32 PM
 */
@RestController
public class UserController {

    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;


    @DubboReference
//    @Reference(version = "1.0.2", check = false, group = "lwb")
    private OrderDubboService demoService;

    @GetMapping("/useLocalCache")
    public String userCtrl(){
        return useLocalCache+"";
    }

    @RequestMapping("/getRemoteOrder/{v}")
    public String getRemoteOrder(@PathVariable String v){
        return demoService.getDubboMsg(v);
    }


}
