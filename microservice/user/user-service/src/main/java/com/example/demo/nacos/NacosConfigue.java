package com.example.demo.nacos;

import com.alibaba.nacos.api.annotation.NacosProperties;
import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

/**
 * @author afeng
 * @des
 * @date 2021/8/21 6:29 PM
 */
@EnableDiscoveryClient
@Configuration
//@NacosConfigurationProperties(dataId = "example.properties")
@NacosPropertySource(dataId = "example.properties", autoRefreshed = true)
public class NacosConfigue {

    public static void main(String[] args) {
        String fileUrlPrefix = ResourceUtils.FILE_URL_PREFIX; //todo 耦合了, 一般自己用自己的util
    }
}
