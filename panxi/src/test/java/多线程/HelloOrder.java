package 多线程;

/**
 * @description: 秒杀
 * @author: liqf
 * @create: 2021-08-01 11:49
 **/
public class HelloOrder implements Runnable{
    private int num = 1000;

    @Override
    public void run() {
        while (true) {
            if (num < 0) {
                break;
            }
            System.out.println("num = " + num);
            num--;
        }
    }

    public static void main(String[] args) {
        HelloOrder helloOrder = new HelloOrder();
        new Thread(helloOrder).start();
        new Thread(helloOrder).start();
        new Thread(helloOrder).start();
    }
}

