package 多线程;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @description: 网图下载
 * @author: liqf
 * @create: 2021-08-01 11:09
 **/
public class ImageDownLoad2 implements Runnable {
    private String name;
    private String url;

    public ImageDownLoad2(String url, String name) {
        this.name = name;
        this.url = url;
    }

    @Override
    public void run() {
        DownLoader downLoader = new DownLoader();
        downLoader.down(name,url);
    }

    public static void main(String[] args) {
        new Thread(new ImageDownLoad("https://img0.baidu.com/it/u=3101694723,748884042&fm=26&fmt=auto&gp=0.jpg","0.jpg")).start();
    }

    class DownLoader{

        public void down(String name, String url){
            try {
                System.out.println("name = " + name);
                FileUtils.copyURLToFile(new URL(url), new File(name));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}



