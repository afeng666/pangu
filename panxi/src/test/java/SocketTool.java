import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * @description:
 * @author: liqf
 * @create: 2021-07-24 13:41
 **/
public class SocketTool {

    /**
     * 端口测试程序, 类似于telnet工具, java亦可实现
     *
     * @throws IOException
     */
    @org.junit.jupiter.api.Test
    void name() throws IOException {
        Socket socket = new Socket();
        SocketAddress socketAddress = new InetSocketAddress("xin", 22);
        socket.connect(socketAddress,3000);
        System.out.println("socket = " + socket);
        System.out.println("socket.getRemoteSocketAddress() = " + socket.getRemoteSocketAddress());
        System.out.println("socket.getLocalAddress() = " + socket.getLocalAddress());
        System.out.println("socket.isConnected() = " + socket.isConnected());
    }


}

