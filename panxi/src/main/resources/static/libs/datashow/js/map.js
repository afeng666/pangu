$(function(){
	var worldMapContainer1 = document.getElementById('distribution_map');
	var myChart = echarts.init(worldMapContainer1);
	myChart.hideLoading();

var option = {
		tooltip: {
			trigger: 'item'
		},
		legend: {
			orient: 'vertical',
			x: 'left',
			y: 'bottom',
			data: [
				'告警数量'
			],
			textStyle: {
				color: '#ccc'
			}
		},
		visualMap: {
			min: 0,
			max: 50,
			left: 'right',
			top: 'bottom',
			text: ['高', '低'], // 文本，默认为数值文本
			calculable: true,
			realtime: false,
			textStyle: {
				color: '#fff'
			}
		},
		series: [{
				name: '告警数量',
				type: 'map',
				aspectScale: 0.75,//地图比例
				zoom: 1.2,
				mapType: '兰州',
				roam: false,//缩放
				label: {
					normal: {
						show: true,//显示标签
						textStyle:{color:"#c71585"}//省份标签字体颜色
					},
					emphasis: {//对应的鼠标悬浮效果
						show: true,
						textStyle:{color:"#800080"}
					}
				},
				itemStyle: {
					normal: {
						borderWidth: .5,//区域边框宽度
						borderColor: '#009fe8',//区域边框颜色
						areaColor:"#ffffff",//区域颜色
					},
					emphasis: {
						borderWidth: .5,
						borderColor: '#4b0082',
						areaColor:"#ffdead",
					}
				},
				data: function() {
					var serie = [];
					for(var i = 0; i < vm.map.length; i++) {
						var item = {
							name: vm.map[i].area,
							value: vm.map[i].cnt
						};
						serie.push(item);
					}
					return serie;
				}()

			}
		]
	};

	myChart.setOption(option,true);

	// 使用刚指定的配置项和数据显示图表。
	myChart.on('click', function (params) {//点击事件
		if (params.componentType === 'series') {
		}
	})
}
)
