layui.define(['exbase', 'jquery', 'layer'], function (exports) {
    var $ = layui.$;
    var exbase = layui.exbase;

    function LayerAjax(data) {
        var _this = this;
        this.url = data.url;
        this.method = data.method || "GET";
        this.contentType = data.contentType || "application/x-www-form-urlencoded";
        this.title = data.title || "标题";
        this.msg = data.msg || "确认执行吗？"
        this.success = data.success;
        this.data = data.data;

        this.open = function () {
            layer.confirm(_this.msg, {icon: 3, title: _this.title}, function (index) {
                //do something
                layui.use("jquery", function () {
                    var submit_data = _this.data;
                    if (_this.contentType == "application/json") {
                        submit_data = JSON.stringify(_this.data);
                    }
                    var loadingIndex = layer.load(1, {
                        shade: [0.1] //0.1透明度的白色背景
                    });
                    $.ajax({
                        url: _this.url,
                        type: _this.method,
                        contentType: _this.contentType,
                        data: _this.data,
                        dataType: 'json',
                        success: function (data) {
                            layer.close(loadingIndex);
                            if (data.code == exbase.AJAX_SUCCESS_CODE) {
                                layer.close(index);
                                if (_this.success) {
                                    _this.success(data);
                                }
                            } else {
                                layer.msg('失败：' + data.msg, {icon: 2});
                            }
                        }
                    });
                });
            });
        }
    }

    exports('LayerAjax', LayerAjax)
});