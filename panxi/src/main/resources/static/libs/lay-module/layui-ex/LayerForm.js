layui.define(['exbase', 'jquery', 'form', 'layer', 'DynamicForm'], function (exports) {
    var $ = layui.$;
    var exbase = layui.exbase;

    function LayerForm(data) {
        var _this = this;
        this.url = data.url;
        this.method = data.method || "GET";
        this.contentType = data.contentType || "application/x-www-form-urlencoded";
        this.title = data.title || "提交表单";
        this.success = data.success;
        this.form = data.form;
        this.area = data.area || "auto";
        this.onload = data.onload;
        this.data = data.data;

        this.form_id = data.form_id || exbase.generateId("layer_form");


        this.open = function () {
            var layer = layui.layer;
            var form = layui.form;
            var $ = layui.jquery;

            var layerForm = new layui.DynamicForm({
                form: _this.form,
                form_id: _this.form_id,
                onload: _this.onload,
                data: _this.data,
                url: _this.url,
                method: _this.method,
                contentType: _this.contentType,
                hideButtons: true,
                success: function (data) {
                    if (data.code == exbase.AJAX_SUCCESS_CODE) {
                        layer.close(_this.index)
                        _this.success(data);
                    } else {
                        layer.msg('失败：' + data.msg, {icon: 2});
                    }
                }
            })

            _this.index = layer.open({
                title: _this.title,
                type: 1,
                area: _this.area,
                content: layerForm.define(),
                btn: ["提交", "取消"],
                yes: function (index, layero) {
                    $("#submit_" + _this.form_id).click()
                    return false;
                }
            })
            layerForm.render();
        }
    }

    exports('LayerForm', LayerForm);
})