layui.define(function(exports){
  //do something
  console.log("demo.js模块示例")

  //demo 就会注册到 layui 对象下，即可通过 var demo = layui.demo 去得到该模块接口
  exports('demo', {
    msg: 'Hello Demo'
  });
});