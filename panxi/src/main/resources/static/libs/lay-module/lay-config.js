/**
 * date:2019/08/16
 * author:Mr.Chung
 * description:此处放layui自定义扩展
 * version:2.0.4
 */

window.rootPath = (function (src) {
    src = document.scripts[document.scripts.length - 1].src;
    console.log("lay-config.js的路径",src)
    return src.substring(0, src.lastIndexOf("/") + 1);
})();

layui.config({
//    base: rootPath + "lay-module/",
    base: rootPath,
    version: false //一般用于更新模块缓存，默认不开启。设为 true 即让浏览器不缓存。也可以设为一个固定的值，如：201610
    ,debug: true //用于开启调试模式，默认 false，如果设为 true，则JS模块的节点会保留在页面

}).extend({ //拓展一个模块别名，如：layui.extend({test: '/res/js/test'})

    treeTable: 'treetablelay/treeTable', //table树形扩展
    miniAdmin: "layuimini/miniAdmin", // layuimini后台扩展
    miniMenu: "layuimini/miniMenu", // layuimini菜单扩展
    miniTab: "layuimini/miniTab", // layuimini tab扩展
    miniTheme: "layuimini/miniTheme", // layuimini 主题扩展
    miniTongji: "layuimini/miniTongji", // layuimini 统计扩展
    step: 'step-lay/step', // 分步表单扩展
    tableSelect: 'tableSelect/tableSelect', // table选择扩展
    iconPickerFa: 'iconPicker/iconPickerFa', // fa图标选择扩展
    echarts: 'echarts/echarts', // echarts图表扩展
    echartsTheme: 'echarts/echartsTheme', // echarts图表主题扩展
    wangEditor: 'wangEditor/wangEditor', // wangEditor富文本扩展
    layarea: 'layarea/layarea', //  省市县区三级联动下拉选择器
    DynamicForm: 'layui-ex/DynamicForm', //  动态表单
    exbase: 'layui-ex/Exbase', //  基础
    ExForm: 'layui-ex/ExForm', //  扩展表单
    ExTree: 'layui-ex/ExTree', //  树
    LayerAjax: 'layui-ex/LayerAjax', //  ajax
    LayerForm: 'layui-ex/LayerForm', //  表单
    xmSelect: 'xmSelect/xm-select', //树形Select扩展
//    upload: 'upload/upload', //upload自定义图片上传组件
    demo: 'demo/demo'

});
