package panxi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @description: 首页页面, 目前这儿是单体, 大后期都是玩前后端分离的
 * @author: liqf
 * @create: 2021-06-30 17:33
 **/
@RestController
public class IndexController {

    @RequestMapping({"index",""})
    public ModelAndView index(){
        return new ModelAndView("index");
    }
}

