package panxi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @description:
 * @author: liqf
 * @create: 2021-06-30 17:17
 **/
@SpringBootApplication
@MapperScan("panxi.mapper")
public class PanXiStarter {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(PanXiStarter.class, args);
    }
}

